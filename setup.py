from setuptools import setup, find_packages


setup(
    name='cellulant_checkout_encryption',
    version='3.0.3',
    license='MIT',
    author="Narayan Solanki",
    author_email='platforms@cellulant.io',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    url='https://thanos.cellulant.africa/cellulant-public/tingg-global-merchants/libraries/python/checkout-pip.git',
    keywords='cellulant checkout encryption',
    install_requires=[
          'pycryptodome',
      ],

)
