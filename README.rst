

Cellulant checkout encryption
===============
To ensure the highest level of security, Cellulant only accepts encrypted data in checkout request.

Installing
============

.. code-block:: bash

    pip install cellulant_checkout_encryption

